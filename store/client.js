export const state = () => ({
  client: '',
  phone: '',
  address: '',
  date: ''
})

export const mutations = {
  setClient(state, client) {
    state.client = client;
  },
  setPhone(state, phone) {
    state.phone = phone;
  },
  setAddress(state, address) {
    state.address = address;
  },
  setDate(state, date){
    state.date = date;
  }
}
