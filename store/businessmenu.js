export const state = () => ({
  kits: [],
  currentDish: null
})

export const mutations = {
  setKit(state, kit) {
    state.kits.push(kit);
  },
  deleteKit(state, id) {
    state.kits.splice(id, 1);
  },
  setKitById(state, change) {
    state.kits[change.id] = change.kit;
  },
  setKits(state, kits) {
    state.kits = kits;
  },
  setCurrentDish(state, dish) {
    state.currentDish = dish;
  }
}
