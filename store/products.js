export const state = () => ({
  prodlist: [],
  categories: [],
  classes: [],
  one: {},
  one_error: false,
})

export const mutations = {
  setOneError(state) {
    state.one_error = true
  },
  setOneProd(state, data) {
    state.one = data
  },
  setCategories(state, data) {
    state.categories = data
    mutations.setProdListByCatId(state, state.categories[0].id)
  },

  setProdListByCatId(state, catId) {
    const catIndex = state.categories.findIndex((x) => x.id === catId)
    state.prodlist = state.categories[catIndex].products
  },
}

export const actions = {
  async getAllCategory(context) {
    try {
      const data = await this.$strapi.find('marhaba-categories');
      const desserts = data.find(item => item.name.includes('Десерты'));
      data.shift();
      data.push(desserts);
      context.commit('setCategories', data.filter(item => !item.name.includes('Бизнес-ланч')) );
    } catch (error) {
      this.error = error
    }
  },

  async getOneById(context, id) {
    try {
      const data = await this.$strapi.find('marhaba-items', { id })
      context.commit('setOneProd', data[0])
    } catch (error) {
      context.commit('setOneError')
    }
  },

  async getClasses(context) {
    try {
      return await this.$strapi.find('marhaba-menu-classes')
    } catch (error) {
      context.commit('setOneError')
    }
  },
}
