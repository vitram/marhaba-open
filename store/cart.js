export const state = () => ({
  checkoutPrice: 0,
  amount: 0,
  cartItems: [],
  businessCartItems: []
})

export const mutations = {
  setPriceAndAmount(state) {
    state.amount = 0;
    state.checkoutPrice = 0;

    for (const kit of state.businessCartItems) {
      state.amount += 1;
      state.checkoutPrice += kit.cost;
    }

    state.cartItems.forEach((element) => {
      let price = Number(element.price) * Number(element.amount)
      state.amount += element.amount;
      state.checkoutPrice += price;
    })
  },

  setBusinessCartItems(state, kits) {
    state.businessCartItems = kits;
    mutations.setPriceAndAmount(state);
  },

  addItem(state, product) {
    // Если в корзине есть итем с таким же айдишником, просто прибавляем ему. Если нет - пушим новый объект
    const prodIndex = state.cartItems.findIndex((x) => x.id === product.id)
    if (prodIndex >= 0) {
      state.cartItems[prodIndex].amount++
    } else {
      const preCartProd = { ...product }
      preCartProd.amount = 1
      state.cartItems.push({ ...preCartProd })
    }
    mutations.setPriceAndAmount(state);
  },
  minusItem(state, product) {
    const prodIndex = state.cartItems.findIndex((x) => x.id === product.id)
    if (state.cartItems[prodIndex].amount === 1) {
      // delete
      state.cartItems.splice(prodIndex, 1)
    } else {
      state.cartItems[prodIndex].amount--
    }
    mutations.setPriceAndAmount(state);
  },
  plusItem(state, product) {
    const prodIndex = state.cartItems.findIndex((x) => x.id === product.id)
    state.cartItems[prodIndex].amount++
    mutations.setPriceAndAmount(state);
  },

  deleteItem(state, product) {
    const prodIndex = state.cartItems.findIndex((x) => x.id === product.id)
    state.cartItems.splice(prodIndex, 1)
    mutations.setPriceAndAmount(state);
  },

  addItemWithAmount(state, product) {
    const prodIndex = state.cartItems.findIndex((x) => x.id === product.id)
    if (prodIndex === 0 || prodIndex > 0) {
      state.cartItems[prodIndex].amount =
        state.cartItems[prodIndex].amount + product.amount
    } else {
      state.cartItems.push({ ...product })
    }
    mutations.setPriceAndAmount(state);
  },
}
